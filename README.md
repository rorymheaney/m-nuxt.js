# Project M Plus

> WordCamp Nuxt.js demo with updated packages

## Built With (in additional to the CLI installation)

* @nuxtjs/dotenv
* @nuxtjs/google-anatlyics
* @nuxtjs/redirect-module
* @nuxtjs/robots
* @nuxtjs/sitemap
* @sentry/.. (additional error handling)
* nuxt-env (evn variables in code)
* nuxt-trailingslash-module
* vee-validate
* more but are probably project specifics

## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
